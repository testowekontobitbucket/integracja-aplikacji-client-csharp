﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IntegracjaAplikacjiClient.ServiceReference1;

namespace IntegracjaAplikacjiClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BookManagerClient bookManagerClient = new BookManagerClient();
            
            book[] books = null;
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    books = bookManagerClient.searchByTitle(textBox1.Text);
                    break;
                case 1:
                    books = bookManagerClient.searchByAuthor(textBox1.Text);
                    break;
                case 2:
                    books = new book[1];
                    books[0] = bookManagerClient.searchByISBN(textBox1.Text);
                    break;

            }
            
            dataGridView1.DataSource = books;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            BookManagerClient bookManagerClient = new BookManagerClient();

            book book = new book
            {
                author = textBoxAutor.Text,
                isbn = textBoxISBN.Text,
                pages = int.Parse(textBoxPages.Text),
                title = textBoxTytul.Text,
                year = int.Parse(textBoxYear.Text),
                publisher = textBoxPublisher.Text
            };

            bookManagerClient.addBook(book);
        }
    }
}
